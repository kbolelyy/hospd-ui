import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../services/login-service.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent implements OnInit {

  private loggedIn = false;
  private credential = {'login': '', 'password': ''};

  constructor(private loginService: LoginService, private router: Router) {
  }

  loginUser() {
    console.log(this.credential.login + ' ' + this.credential.password);
    this.loginService.loginUser(this.credential.login, this.credential.password).subscribe(
      res => {
        localStorage.setItem('xAuthToken', res['token']);
        this.loggedIn = true;
        this.router.navigateByUrl('/main');
      },
      error => {
        console.log(error);
      }
    );
  }

  checkSession() {
    this.loginService.checkSession().subscribe(
      res => {
        console.log(res);
      },
      error => {
        console.log(error);
      }
    );
  }

  ngOnInit() {
    this.loginService.checkSession().subscribe(
      res => {
        this.loggedIn = true;
        this.router.navigateByUrl('/main');
      },
      error => {
        this.loggedIn = false;
      }
    );

  }

}
