import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../../../services/login-service.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-main-user-menu',
  templateUrl: './main-user-menu.component.html',
  styleUrls: ['./main-user-menu.component.css']
})
export class MainUserMenuComponent implements OnInit {

  private loggedIn = false;

  constructor(private loginService: LoginService, private router: Router) {
  }


  logout() {
    this.loginService.logout().subscribe(
      res => {
        this.loggedIn = false;
        this.router.navigateByUrl('/login');
      },
      error => {
        console.log(error);
      }
    );
  }

  ngOnInit() {
    this.loginService.checkSession().subscribe(
      res => {
        this.loggedIn = true;
      },
      error => {
        this.loggedIn = false;

      }
    );
  }

}
