import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../../services/login-service.service';
import {UserModel} from '../../../model/user-model';

@Component({
  selector: 'app-user-custom-form',
  templateUrl: './user-custom-form.component.html',
  styleUrls: ['./user-custom-form.component.css']
})
export class UserCustomFormComponent implements OnInit {

  public user: UserModel = new UserModel();


  constructor(private loginService: LoginService) {
  }


  createUser() {
    this.loginService.createUser(this.user);
  }

  ngOnInit() {
  }

}
