import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAdmissionToDoctorComponent } from './form-admission-to-doctor.component';

describe('FormAdmissionToDoctorComponent', () => {
  let component: FormAdmissionToDoctorComponent;
  let fixture: ComponentFixture<FormAdmissionToDoctorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormAdmissionToDoctorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAdmissionToDoctorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
