import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

/*material*/
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatIconModule,
  MatMenuModule,
  MatOptionModule,
  MatStepperModule,
  MatPaginatorModule, MatSortModule, MatTableModule
} from '@angular/material';

import {MatFormFieldModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import {MatCheckboxModule} from '@angular/material';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatGridListModule} from '@angular/material';
import {MatSelectModule} from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';

/*animation*/
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

/*components*/
import {AppComponent} from './app.component';
import {LoginComponent} from './components/login/login.component';
import {UserCustomFormComponent} from './components/forms/user-custom-form/user-custom-form.component';
import {MainUserMenuComponent} from './components/main/menu/main-user-menu/main-user-menu.component';
import {FormAdmissionToDoctorComponent} from './components/forms/form-admission-to-doctor/form-admission-to-doctor.component';

/*services*/
import {LoginService} from './services/login-service.service';

/*routing*/
import {routing} from './app.routes';
import {HttpClientModule} from '@angular/common/http';
import {SpecialistTableComponent} from './admin-portal/components/specialist-table/specialist-table.component';
import {SpecialistControlService} from './admin-portal/services/specialist-control-service/specialist-control.service';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserCustomFormComponent,
    MainUserMenuComponent,
    FormAdmissionToDoctorComponent,
    SpecialistTableComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    /*material*/
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    NoopAnimationsModule,
    MatToolbarModule,
    MatGridListModule,
    MatOptionModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMenuModule,
    MatStepperModule,
    MatAutocompleteModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    /*routing*/
    routing,

  ],
  providers: [
    LoginService,
    SpecialistControlService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
