import {Specialist} from '../model/specialist';

/*Здесь хранятся моканные данные для тестирования отображения данных*/
export class MockData {

  static specialistDataMock: Specialist[] = [
    {
      id: 1,
      name: 'Mr doctor1',
      profession: 'Врач1',
      contact: 'kfjla@df.com'
    },
    {
      id: 2,
      name: 'Mr doctor2',
      profession: 'Врач2',
      contact: 'kfjla@df2.com'
    },
    {
      id: 3,
      name: 'Mr doctor3',
      profession: 'Врач3',
      contact: 'kfjla@df3.com'
    },
    {
      id: 4,
      name: 'Mr doctor4',
      profession: 'Врач4',
      contact: 'kfjla@df4.com'
    },
    {
      id: 5,
      name: 'Mr doctor5',
      profession: 'Врач5',
      contact: 'kfjla@df5.com'
    }
  ];
}
