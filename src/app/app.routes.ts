import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {UserCustomFormComponent} from './components/forms/user-custom-form/user-custom-form.component';
import {MainUserMenuComponent} from './components/main/menu/main-user-menu/main-user-menu.component';
import {FormAdmissionToDoctorComponent} from './components/forms/form-admission-to-doctor/form-admission-to-doctor.component';
import {SpecialistTableComponent} from './admin-portal/components/specialist-table/specialist-table.component';

const appRoutes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'registration', component: UserCustomFormComponent},
  {path: 'main', component: MainUserMenuComponent},
  {path: 'create-form-admission', component: FormAdmissionToDoctorComponent},
  {path: 'specialist-table', component: SpecialistTableComponent}

];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
