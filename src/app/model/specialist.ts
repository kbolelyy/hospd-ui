export class Specialist {

  public id?: number;
  public name?: String;
  public profession?: String;
  public contact?: String;
}
