import {Injectable, OnInit} from '@angular/core';
import {UserModel} from '../model/user-model';
import {AppComponent} from '../app.component';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export class LoginService implements OnInit {
  ngOnInit(): void {
  }

  constructor(private http: HttpClient) {
  }


  loginUser(login: String, password: String) {
    const url = AppComponent.appPath + 'token';
    const credentials = btoa(login + ':' + password);
    const basicHeader = 'Basic ' + credentials;
    const headers = new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded',
      'Authorization': basicHeader
    });
    return this.http.get(url, {headers: headers});

  }

  createUser(user: UserModel) {
  }


  checkSession() {
    const url = AppComponent.appPath + 'checkSession';
    const headers = new HttpHeaders({
      'X-Auth-Token': localStorage.getItem('xAuthToken')
    });
    return this.http.get(url, {headers: headers});
  }

  logout() {
    const url = AppComponent.appPath + 'logout';
    const headers = new HttpHeaders({
      'X-Auth-Token': localStorage.getItem('xAuthToken')
    });
    return this.http.post(url, '', {headers: headers});
  }

}
