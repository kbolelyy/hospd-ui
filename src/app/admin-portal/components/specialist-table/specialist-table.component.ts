import {Component, OnInit, ViewChild} from '@angular/core';
import {SpecialistControlService} from '../../services/specialist-control-service/specialist-control.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';


@Component({
  selector: 'app-specialist-table',
  templateUrl: './specialist-table.component.html',
  styleUrls: ['./specialist-table.component.css']
})
export class SpecialistTableComponent implements OnInit {

  public dataSource = new MatTableDataSource();
  public displayedColumns: string[] = ['id', 'name', 'profession', 'contact'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;


  constructor(private specialistService: SpecialistControlService) {
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


  ngOnInit() {

  }

  /*Need for pagination and sorting*/
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

}




