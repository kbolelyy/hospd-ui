import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialistTableComponent } from './specialist-table.component';

describe('SpecialistTableComponent', () => {
  let component: SpecialistTableComponent;
  let fixture: ComponentFixture<SpecialistTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialistTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialistTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
