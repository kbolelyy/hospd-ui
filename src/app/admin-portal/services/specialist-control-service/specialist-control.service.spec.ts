import { TestBed, inject } from '@angular/core/testing';

import { SpecialistControlService } from './specialist-control.service';

describe('SpecialistControlService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SpecialistControlService]
    });
  });

  it('should be created', inject([SpecialistControlService], (service: SpecialistControlService) => {
    expect(service).toBeTruthy();
  }));
});
