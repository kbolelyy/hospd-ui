import {Injectable} from '@angular/core';
import {AppComponent} from '../../../app.component';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class SpecialistControlService {

  constructor(private http: HttpClient) {
  }


  getSpecialists(sort: string, field: string, page: number, limit: number): Observable<any> {
    let url = AppComponent.appPath + `specialist/getSpecialistList/${page}/${limit}/${field}/${sort}`;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Auth-Token': localStorage.getItem('xAuthToken')
    });

    return this.http.get(url, {headers: headers});
  }

}
